# esencmp

This package runs ES encompassing tests for competing forecasts.  It contains the tests proposed in Dimitriadis and Schnaitmann (2021). It allows for different link functions an testing on the boundary as in Dimitriadis, Liu and Schnaitmann (2021+).


Installation
------------

### GitLab (development)

The latest version of the package is under development at
[GitLab](https://gitlab.com/schnaitmannj/esencmp). You can install the
development version using these commands:

    install.packages("devtools")
    devtools::install_gitlab("schnaitmannj/esencmp")

This package requires the [esregression](https://gitlab.com/schnaitmannj/esregression) package which can be installed using

    devtools::install_gitlab("schnaitmannj/esregression")

Examples
--------

    # Load the esencmp package
    library(esencmp)

    # Simulate returns from a GARCH(1,1) model with normal innovations.
    # Get VaR and ES forecasts from a GARCH(1,1) (true model) and a GJR-GARCH(1,1)

    alpha <- 0.025

    GARCH_spec_1 <- rugarch::ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)),
    fixed.pars=list(omega=0.04, alpha1=0.1, beta1=0.85) )
    GARCH_spec_2 <- rugarch::ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)),
    fixed.pars=list(omega=0.04, alpha1=0.05, beta1=0.8, gamma1 = 0.1), variance.model = list(model = "gjrGARCH"))

    GARCH_sim_1  <- rugarch::ugarchpath(GARCH_spec_1 , n.sim=2000, m.sim=1, n.start=1000, startMethod="unconditional")
    VaR_FC_1     <- qnorm(alpha) * rugarch::sigma(GARCH_sim_1)
    ES_FC_1      <- - dnorm(qnorm(alpha))/alpha * rugarch::sigma(GARCH_sim_1)

    GARCH_sim_2  <- rugarch::ugarchpath(GARCH_spec_2 , n.sim=2000, m.sim=1, n.start=1000, startMethod="unconditional")
    VaR_FC_2     <- qnorm(alpha) * rugarch::sigma(GARCH_sim_2)
    ES_FC_2      <- -dnorm(qnorm(alpha))/alpha * rugarch::sigma(GARCH_sim_2)

    # simulated returns
    r <- rnorm(2000) * as.numeric(rugarch::sigma(GARCH_sim_1))

    # VaR and ES forecasts
    Xq <- cbind(VaR_FC_1,VaR_FC_2)
    Xe <- cbind(ES_FC_1,ES_FC_2)

    # Run ES encompassing tests for linear link function
    # i.e. using asymptotic test
    test_linear <- ESEncmp.Test(y = r, xq = Xq, xe = Xe, alpha = alpha, link = "linear", CF = 1, str = TRUE)

    print("ES encompassing tests for linear link function - p-values")
    print("Joint VaR and ES test")
    print("H01 = (1,0,1,0) - first pair encompasses the second")
    print("H02 = (0,1,0,1) - second pair encompasses the first")

    print(cbind(H01 = test_linear$joint$test.out[1,2], H02 = test_linear$joint$test.out[2,2]))

    print("Auxiliary ES test")
    print("H01 = (1,0) - first ES forecast encompasses the second")
    print("H02 = (0,1) - second ES forecast encompasses the first")

    print(cbind(H01 = test_linear$aux$test.out[1,2], H02 = test_linear$aux$test.out[2,2]))

    print("Strict ES test")
    print("H01 = (1,0) - first ES forecast encompasses the second")
    print("H02 = (0,1) - second ES forecast encompasses the first")

    print(cbind(H01 = test_linear$str$test.out[1,2], H02 = test_linear$str$test.out[2,2]))

    # Run ES encompassing tests for convex link function,
    # i.e. testing on the boundary of the parameter space
    test_convex <- ESEncmp.Test(y = r, xq = Xq, xe = Xe, alpha = alpha, link = "convex", CF = 1)

    print("ES encompassing tests for convex link function - p-values")
    print("Joint VaR and ES test")
    print("H01 = (1,1) - first pair encompasses the second")
    print("H02 = (0,0) - second pair encompasses the first")

    print(cbind(H01 = test_convex$joint$test.out[1,2], H02 = test_convex$joint$test.out[2,2]))

    print("Auxiliary ES test")
    print("H01 = (1) - first ES forecast encompasses the second")
    print("H02 = (0) - second ES forecast encompasses the first")

    print(cbind(H01 = test_convex$aux$test.out[1,2], H02 = test_convex$aux$test.out[2,2]))

References
----------

[Forecast Encompassing Test for the Expected Shortfall](https://doi.org/10.1016/j.ijforecast.2020.07.008)

[Encompassing Tests for Value at Risk and Expected Shortfall Multi-Step Forecasts based on Inference on the Boundary](https://arxiv.org/abs/2009.07341)
